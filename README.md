# itandroids_lib-consumer

Example consumer of ITAndroids library through Conan

* Tested with itandroids_lib-0.0.0-beta
* Tested with conan-1.24.0

# Instructions

First time setup:

```sh
conan remote add https://api.bintray.com/conan/itandroids-projects/itandroids_lib itandroids_lib
```

Build and execute consumer:

```sh
mkdir build && cd build
conan install .. --build=missing
conan build ..
./bin/consumer
```