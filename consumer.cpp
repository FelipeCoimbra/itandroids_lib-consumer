#include <iostream>
#include <itandroids/geometry/Vector2D.h>

int main() {
    std::cout << "Hello World!" << std::endl;

    auto helloVector = itandroids_lib::geometry::Vector2D(2, 0);
    std::cout << "Hello itandroids_lib: "<<helloVector.x<<", "<<helloVector.y<<std::endl;
    return 0;
}