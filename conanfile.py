# -*- encoding=utf8 -*-
from conans import ConanFile, CMake

class ITAndroidsLibConsumerConan(ConanFile):
    name = 'itandroids_lib_consumer'
    license = 'MIT'
    settings = 'os', 'compiler', 'arch', 'build_type'
    requires = [
        ('itandroids_lib/0.0.0-beta@felipecoimbra/testing')
    ]
    default_options = {
        'itandroids_lib:shared': True # Flip this to use itandroids_lib as static_lib
    }
    generators = 'cmake'


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()